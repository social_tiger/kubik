﻿using UnityEngine;

public class KubikSpawner : MonoBehaviour {

	[SerializeField]
	private int KubikAmount = 200;
	[SerializeField]
	private int SpawnZoneWidth = 50;
	[SerializeField]
	private GameObject Kubik;
	[SerializeField]
	private Camera cam;


	private Pool pool;
	private int actualAmount = 0;
	private float updTime = 1f;
	private float upd = 1f;

	private RaycastHit hit;
	private Ray ray;
	private Vector3 centerOfScreen;

	void Start () {
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		pool = new Pool(KubikAmount, Kubik);
		centerOfScreen = new Vector3(cam.pixelWidth/2, cam.pixelHeight/2, 0);

		for(int i = 0; i < KubikAmount; i++) {
			GameObject go = Instantiate(Kubik, new Vector3(0, 0, 0), new Quaternion());	
			go.SetActive(false);
			pool.Put(go);
		}
	}
	
	void Update () {
		/*
		каждую секунду происходит попытка спавна кубика
		*/
		upd -= Time.deltaTime;
		if(upd <= 0) {
			upd = updTime;
			if(actualAmount < KubikAmount) {
				float randx = Random.Range(-SpawnZoneWidth/2, SpawnZoneWidth/2);
				float randz = Random.Range(-SpawnZoneWidth/2, SpawnZoneWidth/2);
				GameObject go = pool.Take();
				if(go != null) {
					go.transform.position = new Vector3(randx + cam.transform.position.x, 5, randz + cam.transform.position.z);	
					go.SetActive(true);
					actualAmount++;
				}
			} 
		}
		if(Input.GetMouseButtonDown(0)) {
			ray = cam.ScreenPointToRay(centerOfScreen);
			if(Physics.Raycast(ray, out hit)) {
				if(hit.collider.tag == "Kubik") {
					hit.transform.gameObject.SetActive(false);
					pool.Put(hit.transform.gameObject);
					actualAmount--;
				}
			}
		}
	}

}
