using System;
using System.Collections.Generic;
using UnityEngine;

public class Pool
{
	private Stack<GameObject> st;
	private int capacity;
	private int numberOfObjectsInPool;

	public Pool(int capacity, GameObject Kubik) {
		this.capacity = capacity;
		this.numberOfObjectsInPool = 0;
		this.st = new Stack<GameObject>();
	}

	/*
	обнуляем позицию и вращение кубика и выдаем его из пула (если еще остались в пуле кубики)
	*/
	public GameObject Take() {
		if(numberOfObjectsInPool != 0) {
			numberOfObjectsInPool--;
			GameObject obj = st.Pop();
			obj.transform.position = Vector3.zero;
			obj.transform.rotation = Quaternion.Euler(0, 0, 0);
			obj.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
			return obj;
		}
		else {
			return null;
		}
	}

	/*
	помещаем кубик в пул
	*/
	public bool Put(GameObject obj) {
		if(numberOfObjectsInPool != capacity) {
			st.Push(obj);
			numberOfObjectsInPool++;
			return true;
		}
		return false;
	}
}