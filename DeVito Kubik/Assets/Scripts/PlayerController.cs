﻿using UnityEngine;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour {

	[SerializeField]
	private float speed = 10;
	[SerializeField]
	private float mouseSense = 5;

	private PlayerMotor motor;

	void Start() {
		motor = GetComponent<PlayerMotor>();
	}

	void Update() {
		float xMov = Input.GetAxisRaw("Horizontal");
		float zMov = Input.GetAxisRaw("Vertical");

		Vector3 movHor = transform.right * xMov;
		Vector3 movVer = transform.forward * zMov;

		Vector3 velocity = (movHor + movVer).normalized * speed;

		motor.Move(velocity);

		float yRot = Input.GetAxisRaw("Mouse X");

		Vector3 rot = new Vector3(0, yRot, 0) * mouseSense;

		motor.Rotate(rot);

		float xRot = Input.GetAxisRaw("Mouse Y");

		Vector3 camrot = new Vector3(xRot, 0, 0) * mouseSense;

		motor.RotateCamera(camrot);
	}
}
